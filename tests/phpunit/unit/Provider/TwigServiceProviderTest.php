<?php
namespace Bolt\Tests\Provider;

use Bolt\Provider\TwigServiceProvider;
use Bolt\Tests\BoltUnitTest;
use Bolt\Twig\SafeEnvironment;
use Twig_Environment;

/**
 * Class to test src/Provider/TwigServiceProvider.
 *
 * @author Ross Riley <riley.ross@gmail.com>
 * @author Carson Full <carsonfull@gmail.com>
 *
 * @covers \Bolt\Provider\TwigServiceProvider
 */
class TwigServiceProviderTest extends BoltUnitTest
{
    public function testProvider()
    {
        $app = $this->getApp();
        $app->register(new TwigServiceProvider());
        $app->boot();

        $this->assertNotEmpty($app['twig.options']['cache'], 'Cache path was not set');
        $this->assertInstanceOf(Twig_Environment::class, $app['twig']);
        $this->assertTrue($app['twig']->hasExtension('Bolt'), 'Bolt\Twig\Extension\BoltExtension was not added to twig environment');
        $this->assertContains('bolt', $app['twig.loader.bolt_filesystem']->getNamespaces(), 'bolt namespace was not added to filesystem loader');

        $this->assertInstanceOf(SafeEnvironment::class, $app['safe_twig']);
    }

    public function testConfigCacheDisabled()
    {
        $app = $this->getApp();

        $app['config']->set('general/caching/templates', false);
        $app->register(new TwigServiceProvider());
        $this->assertArrayNotHasKey('cache', $app['twig.options']);
    }
}
